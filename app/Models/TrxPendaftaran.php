<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrxPendaftaran extends Model
{
    protected $table = 'trx_pendaftaran';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    protected $fillable = [
        'profile_id', 'akte', 'kartu_keluarga', 'ijazah', 'metode_pembayaran', 'bank', 'status_pembayaran', 'status_validasi', 'bukti_pembayaran', 'catatan'
    ];

} 