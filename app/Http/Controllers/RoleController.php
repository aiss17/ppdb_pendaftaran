<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Helper\DocumentHelper;

class RoleController extends Controller
{
    public function getAll(Request $request)
    {
        try {
            $datas = Role::orderBy('created_at', 'DESC')->get();

            return $this->responsesuccess("get", $datas);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function getById(Request $request)
    {
        try {
            $data = Role::where('id', $request->id)
                ->first();

            return $this->responsesuccess("get", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function create(Request $request)
    {

        try {
            $data = Role::insert([
                    'role' => $request->role
                ]);
            return $this->responsesuccess("post", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function update(Request $request)
    {
        // $validate = $this->validate($request, [
        //     'id' => 'required',
        //     'instansi' => 'required',
        //     'logo_surat' => 'required'
        // ]);

        $tgl_sekarang = Carbon::now()->toDateTimeString();
        try {
            $data = DB::connection('bkipgsql::read')->table("roles")
                ->where('id', $request->id)
                ->first();

            if ($data == null) {
                return $this->responsesuccess("notfound", $data);
            } else {
                $datas = DB::connection('bkipgsql::write')->table('roles')
                    ->where('id', $request->id)
                    ->update([
                        'item' => $request->item,
                        'updated_at' => $tgl_sekarang,
                        'updated_by' => $request->updated_by
                    ]);

                return $this->responsesuccess("update", $datas);
            }
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function delete(Request $request)
    {
        try {
            $data = DB::connection('bkipgsql::read')->table("roles")
                ->where('id', $request->id)
                ->first();

            if ($data == null) {
                return $this->responsesuccess("notfound", $data);
            } else {
                $datas = DB::connection('bkipgsql::write')->table('roles')
                    ->where('id', $request->id)
                    ->delete();

                return $this->responsesuccess("delete", $datas);
            }
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function getNameById(Request $request) {
        try {
            $data = DB::connection('bkipgsql::read')->table("roles")
                ->where('id', $request->id)
                ->select(
                    'item'
                )
                ->first();

            if ($data != null) {
                return $this->responsesuccess("get", $data);
            } else {
                return $this->responsesuccess("Not Found", $data);
            }
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }
}