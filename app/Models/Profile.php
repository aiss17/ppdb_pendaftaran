<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id', 'nama_lengkap', 'no_telp', 'alamat', 'nama_ayah', 'nama_ibu', 'no_telp_ortu', 'asal_sekolah'
    ];

} 