<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_pendaftaran', function (Blueprint $table) {
            $table->id();
            $table->integer('profile_id')->unsigned();
            $table->string('akte');
            $table->string('kartu_keluarga');
            $table->string('ijazah');
            $table->string('metode_pembayaran');
            $table->string('bank');
            $table->string('status_pembayaran');
            $table->string('status_validasi');
            $table->string('bukti_pembayaran');
            $table->string('catatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_pendaftaran');
    }
};
