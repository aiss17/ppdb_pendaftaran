<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'v1/role'], function () use ($router) {
	$router->post('GetAllRole', ['uses' => 'RoleController@getAll']);
	$router->post('GetRoleById', ['uses' => 'RoleController@getById']);
	$router->post('PostRole', ['uses' => 'RoleController@create']);
	$router->post('UpdateRole', ['uses' => 'RoleController@update']);
	$router->post('DeleteRole', ['uses' => 'RoleController@delete']);
	$router->post('GetNameById', ['uses' => 'RoleController@getNameById']);
});
