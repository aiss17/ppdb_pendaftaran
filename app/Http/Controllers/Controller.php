<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

class Controller extends BaseController
{
    //
    public function responsesuccess($type, $response)
    {
      if ($type === "get") {
          return response()->json(['status' => "Succes", 'message' => "Data berhasil diambil", 'data' => $response], 200);
      } else if ($type === "post") {
          return response()->json(['status' => "Succes", 'message' => "Data berhasil disimpan"], 200);
      } else if ($type === "update") {
          return response()->json(['status' => "Succes", 'message' => "Data berhasil diubah"], 200);
      } else if ($type === "delete") {
          return response()->json(['status' => "Succes", 'message' => "Data berhasil dihapus"], 200);
      } else if ($type === "exists") {
          return response()->json(['status' => "Exists", 'message' => "Data already exists"], 200);
      } else {
          return response()->json(['status' => "Not Found", 'message' => "Data tidak ditemukan"], 200);
      }
    }

    public function responsefail($e)
    {
        $err = FlattenException::create($e);
        return response()->json(['status' => "error", 'message' => $e->getMessage()], $err->getStatusCode());
    }

    public function fileUploadResponseFail($e)
    {        
        return response()->json(['status' => "error", 'message' => $e], 500);
    }
}
